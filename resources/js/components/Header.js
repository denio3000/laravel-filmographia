import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import { withRouter } from 'react-router-dom'
import AuthService from './AuthService'
const Auth = new AuthService()


class Nav extends Component {

    constructor(props){
        super(props);
    }

    logout(e){
        e.preventDefault();
        Auth.logout();
        this.props.history.push('/login');
    }

    handleClick(e) {
        e.preventDefault();
        this.props.history.push('/');

    }

    renderLoggedInBar(){
        if (Auth.loggedIn()) {
            return (
                <ul className="nav navbar-nav navbar-right">
                    <li><h5>Welcome {Auth.getProfile().name}</h5></li>
                    <li><Link className="logout-btn" to="#" onClick={this.logout.bind(this)}>Logout</Link></li>
                </ul>
            )
        }

        return (
            <ul className="nav navbar-nav navbar-right">
                <li><Link to="/login">Login</Link></li>
                <li><Link to="/register">Register</Link></li>
            </ul>
        )
    }

    render() {
        return (
            <nav className="navbar navbar-default">
                <div className="container">
                    <h1 className="navbar-header">
                        <a className="brand" href="#" onClick={this.handleClick.bind(this)}>Filmographia</a>
                    </h1>
                    { this.renderLoggedInBar() }
                </div>
            </nav>
        )
    }

}

export default  withRouter(Nav)