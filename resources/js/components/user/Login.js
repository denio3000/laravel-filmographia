import axios from 'axios'
import React, {Component} from 'react';
import AuthService from '../AuthService';

class Login extends Component {

    constructor() {
        super()
        this.Auth = new AuthService();

        this.state = {
            email: '',
            password: '',
        }
    }

    onSubmit(e) {
        e.preventDefault()

        this.Auth.login(this.state.email, this.state.password)
            .then(response => {

                if (response.success) {
                    this.setState({err: false});
                    this.props.history.replace('/')
                } else {
                    this.setState({err: true, msg: response.data});
                    console.log('err', response)
                }
            })
            .catch(err => {
                this.setState({err: true, msg: err});
                this.state.msg = err;
                alert('err', err)
            })
    }

    onChange(e) {
        const {name, value} = e.target;
        this.setState({[name]: value});
    }

    componentWillMount() {
        if (this.Auth.loggedIn())
            this.props.history.replace('/');
    }

    handleChange(e) {
        this.setState(
            {
                [e.target.name]: e.target.value
            }
        )
    }

    render() {
        let error = this.state.err;
        let msg = (error) ? this.state.msg : null;
        let name = (!error) ? 'alert alert-success' : 'alert alert-danger';

        return (
            <div className="container">
                <div className="row">
                    <div className="col-sm-8 center">
                        <h1>Login</h1>
                        <div className="col-md-offset-2 col-md-8 col-md-offset-2">
                            {error != undefined && <div className={name} role="alert">{msg}</div>}
                        </div>
                        <form className="form-horizontal" role="form" method="POST" onSubmit={this.onSubmit.bind(this)}>
                            <div className="form-item form-group">
                                <input className="form-control" placeholder="email" name="email" type="text"
                                       onChange={this.onChange.bind(this)}/>
                            </div>
                            <div className="form-item form-group">
                                <input className="form-control" placeholder="Password" name="password" type="password"
                                       onChange={this.onChange.bind(this)}/>
                            </div>
                            <div className="form-item form-group">
                                <input className="form-submit" value="SUBMIT" type="submit"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;