import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import AuthGuard from '../user/AuthGuard'

class FilmsList extends Component {
    constructor () {
        super()
        this.state = {
            films: [],
        }
    }

    componentDidMount () {
        axios.get('/api/films').then(response => {
            this.setState({
                films: response.data.data
            })
        })
    }

    renderImage(){
        const {film} = this.state

        if(film.photo){
            return (
                <div className="film-image col-sm-3">
                    <img src={film.photo} />
                </div>
            )
        }
    }

    render () {
        if (this.state.films) {
        const { films } = this.state

        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-md-12'>
                        <div className='film-list-container'>
                            <div className='film-header clearfix'>
                                <h3 className='float-left'>Discover your favourite movies</h3>
                                <Link className='btn btn-primary float-right' to='/create'>Create new film</Link>
                            </div>
                            <div className='film-body'>
                                <ul className='film-container-list'>
                                    {films.map((film) =>

                                        <li key={film.id} className='film-list-item clearfix'>
                                            <div className="row">
                                                <div className="col-sm-2 imageContainer">
                                                    <img src={film.photo ? film.photo: 'https://via.placeholder.com/160x170'} alt=""/>
                                                </div>
                                                <div className="col-sm-10">
                                                    <h3 className='title'>
                                                        <Link to={`film/${film.id}`}>{film.name}</Link>
                                                    </h3>
                                                    <div  className='description clearfix'>{film.description}</div>
                                                    <span className='tag ticket-price'>Price: <span>{film.ticket_price}</span></span>
                                                    <span className='tag country'>Country: <span>{film.country}</span></span>
                                                    <span className='tag genre'>Genre: <span>{film.genre}</span></span>
                                                    <span className='tag release-date'>Release Date: <span>{new Date(film.release_date).getMonth()+1} {new Date(film.release_date).getFullYear()}</span></span>
                                                </div>
                                            </div>
                                        </li>
                                    )}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
        }
    }
}

export default AuthGuard(FilmsList)