import axios from 'axios'
import React, {Component} from 'react'
import AuthService from '../AuthService'

class FilmDetail extends Component {
    constructor(props) {
        super(props)

        this.Auth = new AuthService
        this.state = {
            filmId: this.props.match.params.id,
            film: {}
        }

        this.handleRateFilm = this.handleRateFilm.bind(this)
        this.hasErrorFor = this.hasErrorFor.bind(this)
        this.renderErrorFor = this.renderErrorFor.bind(this)
    }

    componentDidMount() {
        const filmId = this.state.filmId
        const $rateBtn = document.getElementById('rating-system')

        axios.get(`/api/films/${filmId}`).then(response => {
            this.setState({
                film: response.data.data
            })
        })

        // Check your rating on init
        axios.post(`/api/rating/check`, {
            film_id: filmId,
            token: this.Auth.getToken()
        }).then(response => {

            if(response.status === 200) {
                if(response.data.data) {
                    $rateBtn.innerHTML = 'Your rating: ' + response.data.data.rating
                }
            }
        }).catch(err => {
            console.log('err', err)
        })
    }

    handleRateFilm(e) {
        const $rateBtn = e.target.parentNode
        const ratingVal = e.target.value

        axios.post(`/api/rating/create`, {
            film_id: this.state.filmId,
            rating: ratingVal,
            token: this.Auth.getToken()
        }).then(response => {

            console.log(response)
            if(response.data.data) {
                $rateBtn.innerHTML = 'Thanks for rating!'
            }
        })
    }

    hasErrorFor(field) {
        return !!this.state.errors[field]
    }

    renderErrorFor(field) {
        if (this.hasErrorFor(field)) {
            return (
                <span className='invalid-feedback'>
            <strong>{this.state.errors[field][0]}</strong>
          </span>
            )
        }
    }

    renderImage(){
        const {film} = this.state

        if(film.photo){
            return (
                <div className="film-image col-sm-3">
                    <img src={film.photo} />
                </div>
            )
        }else{
            return(
                <div className="film-image col-sm-3">
                    <img src="https://via.placeholder.com/150" />
                </div>
            )
        }
    }

    render() {

        if (this.state.film) {
            const {film} = this.state

            return (
                <div className='container'>
                    <div className='row'>
                        <div className='col-md-9'>
                            <div className='film row'>
                                {this.renderImage()}
                                <div className='film-body col-sm-9'>
                                    <h1 className='film-header'>{film.name}</h1>
                                    <div className='description'>{film.description}</div>
                                    <hr/>
                                    <div className='tag price-tag'>Price: <span>{film.ticket_price}</span></div>
                                    <div className='tag country-tag'>Country: <span>{film.country}</span></div>
                                    <div className='tag genre-tag'>Genre: <span>{film.genre}</span></div>
                                    <div className='tag release-date-tag'>Release Date:
                                        <span>{new Date(film.release_date).getMonth() + 1} {new Date(film.release_date).getFullYear()}</span>
                                    </div>

                                    <div id="rating-system" className='rating-system'>
                                        <button className='rate-movie' value="1"
                                                onClick={this.handleRateFilm.bind(this)}>★
                                        </button>
                                        <button className='rate-movie' value="2"
                                                onClick={this.handleRateFilm.bind(this)}>★
                                        </button>
                                        <button className='rate-movie' value="3"
                                                onClick={this.handleRateFilm.bind(this)}>★
                                        </button>
                                        <button className='rate-movie' value="4"
                                                onClick={this.handleRateFilm.bind(this)}>★
                                        </button>
                                        <button className='rate-movie' value="5"
                                                onClick={this.handleRateFilm.bind(this)}>★
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
    }
}

export default FilmDetail