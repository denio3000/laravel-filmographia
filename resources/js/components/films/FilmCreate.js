import axios from 'axios'
import React, { Component } from 'react'
import AuthService from '../AuthService'

class FilmCreate extends Component {
    constructor (props) {
        super(props)
        this.state = {
            name: '',
            description: '',
            ticket_price: '',
            country: '',
            genre: '',
            photo: '',
            errors: []
        }
        this.Auth = new AuthService
        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.handleCreateFilm = this.handleCreateFilm.bind(this)
        this.hasErrorFor = this.hasErrorFor.bind(this)
        this.renderErrorFor = this.renderErrorFor.bind(this)
    }

    handleFieldChange (e) {
        console.log(e.target)
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleCreateFilm (event) {
        event.preventDefault()

        const { history } = this.props

        const film = {
            name: this.state.name,
            description: this.state.description,
            ticket_price: this.state.ticket_price,
            genre: this.state.genre,
            country: this.state.country,
            photo: this.state.photo,
            token: this.Auth.getToken()
        }

        axios.post('/api/films', film)
            .then(response => {
                console.log('resp', response);
                // redirect to the homepage
                if(response.data.data) {
                    history.push('/')
                }
            })
            .catch(error => {
                console.log('err', error);
                // this.setState({
                //     errors: error.response.data.errors
                // })
            })
    }

    hasErrorFor (field) {
        return !!this.state.errors[field]
    }

    renderErrorFor (field) {
        if (this.hasErrorFor(field)) {
            return (
                <span className='invalid-feedback'>
              <strong>{this.state.errors[field][0]}</strong>
            </span>
            )
        }
    }

    render () {
        return (
            <div className='container'>
                <div className='row justify-content-center'>
                    <div className='col-md-6'>
                        <div className='card'>
                            <div className='card-header'>Create new film</div>
                            <div className='card-body'>
                                <form onSubmit={this.handleCreateFilm}>
                                    <div className='form-group'>
                                        <label htmlFor='name'>Film name</label>
                                        <input
                                            id='name'
                                            type='text'
                                            className={`form-control ${this.hasErrorFor('name') ? 'is-invalid' : ''}`}
                                            name='name'
                                            value={this.state.name}
                                            onChange={this.handleFieldChange}
                                        />
                                        {this.renderErrorFor('name')}
                                    </div>
                                    <div className='form-group'>
                                        <label htmlFor='ticket_price'>Ticket price</label>
                                        <input
                                            id='ticket_price'
                                            type='number'
                                            className={`form-control ${this.hasErrorFor('ticket_price') ? 'is-invalid' : ''}`}
                                            name='ticket_price'
                                            value={this.state.ticket_price}
                                            onChange={this.handleFieldChange}
                                        />
                                        {this.renderErrorFor('ticket_price')}
                                    </div>
                                    <div className='form-group'>
                                        <label htmlFor='country'>Country</label>
                                        <input
                                            id='country'
                                            type='text'
                                            className={`form-control ${this.hasErrorFor('country') ? 'is-invalid' : ''}`}
                                            name='country'
                                            value={this.state.country}
                                            onChange={this.handleFieldChange}
                                        />
                                        {this.renderErrorFor('ticket_price')}
                                    </div>
                                    <div className='form-group'>
                                        <label htmlFor='genre'>Genre</label>
                                        <input
                                            id='genre'
                                            type='text'
                                            className={`form-control ${this.hasErrorFor('genre') ? 'is-invalid' : ''}`}
                                            name='genre'
                                            value={this.state.genre}
                                            onChange={this.handleFieldChange}
                                        />
                                        {this.renderErrorFor('ticket_price')}
                                    </div>

                                    <div className='form-group'>
                                        <label htmlFor='photo'>Photo url</label>
                                        <input
                                            id='photo'
                                            type='text'
                                            className={`form-control ${this.hasErrorFor('photo') ? 'is-invalid' : ''}`}
                                            name='photo'
                                            value={this.state.photo}
                                            onChange={this.handleFieldChange}
                                        />
                                        {this.renderErrorFor('ticket_price')}
                                    </div>

                                    <div className='form-group'>
                                        <label htmlFor='description'>Film description</label>
                                        <textarea
                                            id='description'
                                            className={`form-control ${this.hasErrorFor('description') ? 'is-invalid' : ''}`}
                                            name='description'
                                            rows='10'
                                            value={this.state.description}
                                            onChange={this.handleFieldChange}
                                        />
                                        {this.renderErrorFor('description')}
                                    </div>
                                    <button className='btn btn-primary'>Create</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default FilmCreate