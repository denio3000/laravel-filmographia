Steps for install:
Execute above commands on the root of project:

1. Install vendors of Laravel
composer install

2. Generate keys for JWT Auth library
php artisan jwt:generate
php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\JWTAuthServiceProvider"

3. Install node packages
npm i

4. php artisan serve
5. Open browser and run http://localhost:8000/
