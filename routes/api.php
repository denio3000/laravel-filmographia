<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/* all routes to protected resources are registered here */
Route::group(['middleware' => ['jwt.auth','api-header']], function () {

    Route::post('rating/create', 'RatingController@store');
    Route::post('rating/check', 'RatingController@check');
});

/* No Login needed // only through header token */
Route::group(['middleware' => 'api-header'], function () {
    // The registration and login requests doesn't come with tokens
    Route::post('user/login', 'UserController@login');
    Route::post('user/register', 'UserController@register');
});

Route::resource('films', 'FilmController');
