<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class FilmsTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,3) as $index) {
            DB::table('films')->insert([
                'name' => $faker->name,
                'description' => $faker->paragraph,
                'release_date' => $faker->dateTime(),
                'ticket_price' => $faker->numberBetween(20, 50),
                'country' => $faker->country,
                'genre' => $faker->word,
                'photo' => '',
                'user_id' => 1,
            ]);
        }
    }
}
