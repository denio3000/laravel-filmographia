<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Film;
use App\Rating;
use App\Http\Resources\RatingResource;
use Tymon\JWTAuth\Facades\JWTAuth;

class RatingController extends Controller
{
    public function store(Request $request)
    {
        $user = JWTAuth::toUser($request->token);
        $rating = Rating::firstOrCreate(
            [
                'user_id' => $user->id,
                'film_id' => $request->film_id,
            ],
            ['rating' => $request->rating]
        );
        return new RatingResource($rating);
    }

    public function check(Request $request)
    {
        $user = JWTAuth::toUser($request->token);
        $rating = Rating::where(
            [
                'user_id' => $user->id,
                'film_id' => $request->film_id,
            ]
        )->first();

        return empty($rating) ? null : new RatingResource($rating);
    }
}
