<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Film;
use App\Http\Resources\FilmResource;
use Carbon\Carbon;
use Tymon\JWTAuth\Facades\JWTAuth;

class FilmController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth')->except(['index', 'show']);
    }

    public function index()
    {
        $nr_per_page = 1;
        return FilmResource::collection(Film::with('ratings')->paginate($nr_per_page));
    }

    public function store(Request $request)
    {
        $release_date = $request->release_date ? Carbon::createFromFormat('d/m/Y', $request->release_date)->format('Y-m-d H:i:s') : null;
        $user = JWTAuth::toUser($request->token);

        $Film = Film::create([
            'user_id'       => $user->id,
            'name'          => $request->name,
            'description'   => $request->description,
            'release_date'  => $release_date,
            'ticket_price'  => $request->ticket_price,
            'country'       => $request->country,
            'genre'         => $request->genre,
            'photo'         => $request->photo,
        ]);

        return new FilmResource($Film);
    }

    public function show(Film $Film)
    {
        return new FilmResource($Film);
    }

    public function update(Request $request, Film $Film)
    {

        $Film->update($request->only([ 'name', 'description', 'release_date', 'ticket_price', 'country', 'genre', 'photo']));
        return new FilmResource($Film);
    }

    public function destroy(Film $Film)
    {
        $Film->delete();

        return response()->json(null, 204);
    }
}
